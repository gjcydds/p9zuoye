from flask import jsonify

import db


def data_list(data):
    list2=[]
    if not len(data):
        return list2

    dict2 = {}
    for i in data:
        dict2[i["code"]] = i

    for j in data:
        pid = j["pid"]
        if not pid:
            list2.append(j)
        else:
            if "son" not in dict2[pid]:
                dict2[pid]["son"] = []
            dict2[pid]["son"].append(j)

    return list2

@addr_bp.rote('/addr')
def index():
    data = db.find_all(f"select * from tb_areas")
    list2 = data_list(data)
    return jsonify({"code":200,"list":list2})