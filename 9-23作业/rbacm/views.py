#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2020/7/13 13:37
# @Author  : CoderCharm
# @File    : views.py
# @Software: PyCharm
# @Github  : github/CoderCharm
# @Email   : wg_python@163.com
# @Desc    :
"""

"""
from typing import Union, Any
from fastapi import APIRouter, Depends, Query
from sqlalchemy.orm import Session

from api.common import deps
from api.common.logger import logger
from api.utils import response_code
# from app.api.api_v1.auth.crud.role import curd_role

from .schemas import goods_schema, role_schema
from .crud.category import curd_category, curd_resource, curd_role_resource

router = APIRouter()


@router.post("/add/role", summary="添加角色")
async def add_role(
        role_info: role_schema.RoleCreate,
        db: Session = Depends(deps.get_db),
):
    print(role_info.name)
    print(db)
    # logger.info(f"添加分类->用户id:，分类名:{role_info.name}")
    curd_category.create(db=db, obj_in=role_info)
    return response_code.resp_200(message="角色添加成功")


@router.get("/get/role", summary="查询角色列表")
async def get_role(
        db: Session = Depends(deps.get_db),
        page: int = Query(1, ge=1, title="当前页"),
        page_size: int = Query(10, le=50, title="页码长度")
):
    logger.info(f"查询分类列表->用户id:,当前页{page}长度{page_size}")
    response_result = curd_category.query_all(db, page=page, page_size=page_size)
    return response_code.resp_200(data=response_result)


# 添加资源
@router.post("/add/resource", summary="添加资源")
async def add_resource(
        resource_info: role_schema.ResourceCreate,
        db: Session = Depends(deps.get_db),
):
    # logger.info(f"添加分类->用户id:，分类名:{role_info.name}")
    curd_resource.create(db=db, obj_in=resource_info)
    return response_code.resp_200(message="资源添加成功")


# 获取资源列表
@router.get("/get/resource", summary="查询资源列表")
async def get_resource(
        db: Session = Depends(deps.get_db),
        page: int = Query(1, ge=1, title="当前页"),
        page_size: int = Query(10, le=50, title="页码长度")
):
    logger.info(f"查询分类列表->用户id:,当前页{page}长度{page_size}")
    response_result = curd_resource.query_all(db, page=page, page_size=page_size)
    return response_code.resp_200(data=response_result)


# # 角色配置资源
# @router.post("/add/role/resource", summary="添加资源列表")
# async def add_resource(
#         resource_info: role_schema.RoleResourceCreate,
#         db: Session = Depends(deps.get_db),
# ):
#     print('？？？？', resource_info)
#     # logger.info(f"添加分类->用户id:，分类名:{role_info.name}")
#     curd_role_resource.create(db=db, obj_in=resource_info)
#     return response_code.resp_200(message="资源列表添加成功")


# 获取资源
@router.get("/get/all/resource", summary="查询资源")
async def get_resource(
        db: Session = Depends(deps.get_db)
):
    logger.info(f"查询分类列表->用户id:,")
    response_result = curd_resource.get_all(db)
    return response_code.resp_200(data=response_result)


# 获取已有的资源
@router.get("/get/role/resource", summary="获取已有的资源")
async def get_role_resource(
        db: Session = Depends(deps.get_db),
        role_id=int
):
    logger.info(f"查询分类列表->用户id:,")
    response_result = curd_resource.role_all(db, role_id=role_id)
    return response_code.resp_200(data=response_result)


# 添加资源列表
@router.post("/post/role/resource", summary="添加资源列表")
async def post_resource(
        resource_info: role_schema.RoleResourceCreate,
        db: Session = Depends(deps.get_db)
):
    print('>>>00>>>>', resource_info)
    logger.info(f"查询分类列表->用户id:,")
    res = curd_resource.role_resource_add(db, resource_info=resource_info)
    if not res:
        return response_code.resp_1001(message='数据已发生了互斥')
    return response_code.resp_200(message="配置成功")

"""
# select res.rname,res.pid,res.url,par.id,par.rname from role_resource as rr left join resource as res on rr.resource_id=res.id left join resource as par on res.pid=par.id where rr.role_id=2;
# select res.rname from role_resource as rr left join resource as res on rr.resource_id=res.id left join resource as par on res.pid=par.id where rr.role_id=2;
"""



# 获取用户对应的权限
@router.get("/get/user/resource", summary="获取用户对应的权限")
async def get_role_resource(
        db: Session = Depends(deps.get_db),
        role_id=int
):
    response_result = curd_resource.user_resource_all(db, role_id=role_id)
    return response_code.resp_200(data=response_result)


